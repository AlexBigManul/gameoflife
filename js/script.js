const startBtn = document.querySelector('.button__start');
let canvas = document.getElementById('world');
let ctx = canvas.getContext('2d');
canvas.width = 1200;
canvas.height = 700;

const cols = 100;
const rows = 100;
const responsiveCell = canvas.width / rows;

let initGeneration = [];

const init = (cols, rows) => {
  const initArray = [];
  for (let i = 0; i < cols; i++) {
    initArray[i] = [];
    for (let j = 0; j < rows; j++) {
      initArray[i][j] = 0;
    }
  }
  return initArray;
};

initGeneration = init(rows, cols);
for (let i = 0; i < cols; i++) {
  for (let j = 0; j < rows; j++) {
    initGeneration[i][j] = (Math.random() / Math.random() > 0.5 ? 0 : 1);
  }
}

const draw = () => {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  for (let i = 0; i < cols; i++) {
    for (let j = 0; j < rows; j++) {
      if (initGeneration[i][j] == 1) {
        ctx.fillRect(
          j * responsiveCell,
          i * responsiveCell,
          responsiveCell,
          responsiveCell,
        );
      }
      ctx.stroke();
    }
  }
};
draw();

const checkValue = (x, y) => {
  try {
    return initGeneration[x][y];
  } catch {
    return 0;
  }
};

const countNeighbors = (x, y) => {
  let sum = 0;
  if (checkValue(x - 1, y)) sum++;
  if (checkValue(x, y + 1)) sum++;
  if (checkValue(x + 1, y)) sum++;
  if (checkValue(x, y - 1)) sum++;
  if (checkValue(x - 1, y + 1)) sum++;
  if (checkValue(+1, y + 1)) sum++;
  if (checkValue(x + 1, y - 1)) sum++;
  if (checkValue(x - 1, y - 1)) sum++;
  return sum;
};

const civilization = () => {

  let nextGeneration = init(rows, cols);
  for (let i = 0; i < cols; i++) {
    nextGeneration[i] = [];
    for (let j = 0; j < rows; j++) {
      nextGeneration[i][j] = (Math.random() / Math.random() > 0.1 ? 0 : 1);
      let state = initGeneration[i][j];
      let neighbors = countNeighbors(i, j);

      if (state == 0 && neighbors == 3) {
        nextGeneration[i][j] = 1;
      } else if (state == 1 && (neighbors < 2 || neighbors > 3)) {
        nextGeneration[i][j] = 0;
      } else {
        nextGeneration[i][j] = state;
      }

    }
  }
  initGeneration = nextGeneration;
  draw();
  setTimeout(civilization, 100);
};

startBtn.addEventListener('click', _ => {
  civilization();
});
